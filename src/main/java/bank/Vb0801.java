package bank;

//Voorbeeld 0801
//Bank, Overerving: Rekening, Betaalrekening, Spaarrekening
public class Vb0801 {

	public static void main(String[] args) {
		Bank javaBank = new Bank();

		Spaarrekening rekening1 = new Spaarrekening("123", 3.0);
		javaBank.voegRekeningToe(rekening1);

		Betaalrekening rekening2 = new Betaalrekening("987", 500);
		javaBank.voegRekeningToe(rekening2);

		rekening1.stort(100);
		rekening1.schrijfRenteBij();
		rekening2.neemOp(45);

		javaBank.printOverzicht();
	}
}
