package bank;

public class Betaalrekening extends Rekening {
	// Constructors
	public Betaalrekening(String rekeningnummer) {
		this.rekeningnummer = rekeningnummer;
		saldo = 0;
	}

	public Betaalrekening(String rekeningnummer, double eersteInleg) {
		this.rekeningnummer = rekeningnummer;
		saldo = eersteInleg;
	}

	// Methoden
	public void print() {
		super.print();
		System.out.println("-------------");
	}
}
